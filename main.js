// Task 1
class Person{
    constructor(name, age) {
        this.name = name,
        this.age = age
    }
}

class Baby extends Person {
    constructor(name, age, favouriteToy) {
        super(name, age) 
        this.favouriteToy = favouriteToy
    }

    play() {
        console.log(`Playing with ${this.favouriteToy}`);
    }
}
const Lazizbek = new Baby("Lazizbek", 19, "Car")
Lazizbek.play()
// console.log(Lazizbek);
// console.log(Lazizbek.play());

// Task 2
class Person1 {
    constructor(name, age) {
        this.name = name,
        this.age = age
    }
    describe() {
        console.log(`${this.name}, ${this.age} years old`);
    }
}
const jack = new Person1("Jack", 25)
const jill = new Person1("Jill", 24)

console.log(jack.describe());
console.log(jill.describe());